<?php
/**
 * @file
 * Definition of views_handler_filter_sentiment_type.
 */

/**
 * Filter by node type.
 *
 * @ingroup views_filter_handlers
 */
class CnectDorisSentiment_views_handler_filter_sentiment_type extends views_handler_filter_in_operator {

  /**
   * Override method get_value_options().
   */
  public function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Sentiment type');
      $options = array(
        'positive' => t("Positive"),
        'neutral' => t("Neutral"),
        'negative' => t("Negative"),
      );
      $this->value_options = $options;
    }
  }

}
