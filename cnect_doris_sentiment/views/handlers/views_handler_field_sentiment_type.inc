<?php
/**
 * @file
 * Definition of views_handler_field_sentiment_type.
 */

/**
 * Field handler to translate a sentiment type into its readable form.
 *
 * @ingroup views_field_handlers
 */
class CnectDorisSentiment_views_handler_field_sentiment_type extends views_handler_field {

  /**
   * Override method render().
   */
  public function render($values) {
    $value = $this->get_value($values);
    return t('@value', array('@value' => drupal_ucfirst($value)));
  }

}
