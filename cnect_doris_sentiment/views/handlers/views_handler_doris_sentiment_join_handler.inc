<?php
/**
 * @file
 * Join handler for doris_sentiment entity.
 */

/**
 * Join handler for doris_sentiment entity.
 */
class CnectDorisSentiment_views_handler_doris_sentiment_join_handler extends views_join {

  /**
   * Build join.
   */
  public function build_join($select_query, $table, $view_query) {
    $left_table = $table['join']->left_table;
    $left_field = $table['join']->left_field;
    $field = $table['join']->field;
    $select_query->addJoin(
      'INNER',
      $this->table, $table['alias'], "{$left_table}.entity_type = '${table['table']}' AND {$left_table}.{$left_field} = ${table['alias']}.${field}");
  }

}
