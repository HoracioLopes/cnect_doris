<?php
/**
 * @file
 * Queue and cron definitions for 'cnect_doris_sentiment'.
 */

/**
 * Implements hook_cron_queue_info().
 */
function cnect_doris_sentiment_cron_queue_info() {
  $queues['cnect_doris_sentiment'] = array(
    'worker callback' => '_cnect_doris_sentiment_process',
    'time' => 30,
  );
  return $queues;
}

/**
 * Implements hook_cron().
 */
function cnect_doris_sentiment_cron() {
  $sentiment_queue = DrupalQueue::get('cnect_doris_sentiment');

  $sentiment_ids = array(0);
  $s_ids = db_select('doris_sentiment', 'ds')
    ->fields('ds', array('entity_id'))
    ->condition('entity_type', 'comment')
    ->execute()
    ->fetchAllAssoc('entity_id');

  if (!empty($s_ids)) {
    $sentiment_ids = array_keys($s_ids);
  }

  $limit = variable_get('cnect_doris_sentiment_comments_per_cron_run', 50);

  $q = db_select('comment', 'c')
    ->fields('c', array('cid'))
    ->condition('cid', $sentiment_ids, 'NOT IN')
    ->range(0, $limit)
    ->execute();

  $cids = $q->fetchCol();

  foreach ($cids as $cid) {
    $comments = entity_load("comment", array($cid));
    $comment = array_pop($comments);

    $sentiment_queue->createItem($comment);
  }
}

/**
 * Worker callback defined in hook_cron_queue_info()
 */
function _cnect_doris_sentiment_process($item) {
  cnect_doris_sentiment_entity_insert($item, 'comment');
}
