-- SUMMARY --

This module provides sentiment analysis on comments.


-- INSTALLATION --

* Install as usual, see http://drupal.org/documentation/install/modules-themes/modules-7
  for further information.


-- CONFIGURATION --

* Config the entity types/fields in which sentiment analysis should be performed
at admin/config/doris

* Existing comments will be analysed when running cron.

* New comments will be analysed at creation time.

