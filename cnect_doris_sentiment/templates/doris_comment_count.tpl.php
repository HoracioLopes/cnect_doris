<?php
/**
 * @file
 * Template file for comment count on sentiment report.
 */
?>
<span class="comment count"><?php print $count ?></span>
<span class="type-label"><?php print $type ?></span>
