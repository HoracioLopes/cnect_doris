<?php
/**
 * @file
 * Template file for average comment sentiment report.
 */
?>
<div class="comment-average-sentiment">
  <div class="count">
    <?php if ($total_comments_breakdown): ?>
      <div class="breakdown"><?php print $total_comments_breakdown ?></div>
    <?php endif; ?>
  </div>
  <div class="row">
    <div id="comments" class="col-lg-12 comment-wrapper most-positive-comments ">
      <?php
        print drupal_render($positive['fieldset']);
      ?>
    </div>

    <div id="comments" class="col-lg-12 comment-wrapper most-negative-comments">
      <?php
        print drupal_render($neutral['fieldset']);
      ?>
    </div>

    <div id="comments" class="col-lg-12 comment-wrapper most-negative-comments">
      <?php
        print drupal_render($negative['fieldset']);
      ?>
    </div>
  </div>
</div>
