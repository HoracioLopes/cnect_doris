<?php
/**
 * @file
 * Theme functions for cnect doris sentiment.
 */

/**
 * Implements hook_theme().
 */
function cnect_doris_sentiment_theme() {
  $module_path = drupal_get_path('module', 'cnect_doris_sentiment');

  return array(
    'doris_comment_sentiment' => array(
      'template' => 'doris_comment_sentiment',
      'path' => $module_path . '/templates',
      'variables' => array(),
    ),
    'doris_comment_count' => array(
      'template' => 'doris_comment_count',
      'path' => $module_path . '/templates',
      'variables' => array(),
    ),
  );
}

/**
 * Preprocess function for doris_comment_sentiment.tpl.php.
 */
function cnect_doris_sentiment_preprocess_doris_comment_sentiment(&$vars) {

  drupal_add_library('system', 'drupal.collapse');

  $view = views_get_view('doris_node_comment_sentiment_views');
  $view->set_display('doris_sentiment_comment_positive');

  $title = $view->get_title();
  $result = $view->preview('doris_sentiment_comment_positive');
  $count = $view->total_rows;

  $vars['positive'] = array(
    'title' => $title,
    'count' => $count,
  );

  $vars['positive']['fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => $title . "({$count})",
    '#value' => $result,
    '#attributes' => array(
      'class' => array(
        'collapsible',
        'collapsed',
      ),
    ),
  );

  $view = views_get_view('doris_node_comment_sentiment_views');
  $view->set_display('doris_sentiment_comment_neutral');

  $title = $view->get_title();
  $result = $view->preview('doris_sentiment_comment_neutral');
  $count = $view->total_rows;

  $vars['neutral'] = array(
    'title' => $title,
    'count' => $count,
  );

  $vars['neutral']['fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => $title . "({$count})",
    '#value' => $result,
    '#attributes' => array(
      'class' => array(
        'collapsible',
        'collapsed',
      ),
    ),
  );

  $view = views_get_view('doris_node_comment_sentiment_views');
  $view->set_display('doris_sentiment_comment_negative');

  $title = $view->get_title();
  $result = $view->preview('doris_sentiment_comment_negative');
  $count = $view->total_rows;

  $vars['negative'] = array(
    'title' => $title,
    'count' => $count,
  );

  $vars['negative']['fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => $title . "({$count})",
    '#value' => $result,
    '#attributes' => array(
      'class' => array(
        'collapsible',
        'collapsed',
      ),
    ),
  );

  $total_comments = $vars['positive']['count'] + $vars['neutral']['count'] + $vars['negative']['count'];
  if (!$total_comments) {
    $vars['total_comments_breakdown'] = t("There are no comments yet.");
  }
  else {
    $breakdown['title'] = t("There are @count comments", array('@count' => $total_comments));
    $breakdown['items'] = array(
      array(
        'data' => theme('doris_comment_count', array('count' => $vars['positive']['count'], 'type' => t('Positive'))),
        'class' => array('positive'),
      ),
      array(
        'data' => theme('doris_comment_count', array('count' => $vars['neutral']['count'], 'type' => t('Neutral'))),
        'class' => array('neutral'),
      ),
      array(
        'data' => theme('doris_comment_count', array('count' => $vars['negative']['count'], 'type' => t('Negative'))),
        'class' => array('negative'),
      ),
    );
    $breakdown['attributes'] = array(
      'class' => array('doris-sentiment-comment-breakdown'),
    );

    $vars['total_comments_breakdown'] = theme('item_list', $breakdown);
  }

}

/**
 * Preprocess comment.
 *
 * Add classes based on sentiment to comments.
 */
function cnect_doris_sentiment_preprocess_comment(&$variables) {
  $comment = $variables['comment'];
  $node = node_load($comment->nid);
  // Verify the user has access to the reports.
  if (cnect_doris_sentiment_report_access('node', $node)) {
    if ($comment->node_type) {

      // Check for enabled fields for this comment bundle.
      $enabled_fields = _cnect_doris_sentiment_is_enabled_bundle($comment->node_type, 'comment');
      if (!empty($enabled_fields)) {

        list($id, $vid, $bundle) = entity_extract_ids('comment', $comment);

        $conditions = array(
          'entity_type' => 'comment',
          'entity_bundle' => $bundle,
          'entity_id' => $id,
          'entity_vid' => $vid,
        );

        $ids = cnect_doris_sentiment_load($conditions);
        if (!empty($ids)) {
          foreach ($ids as $s_entity) {
            $variables['classes_array'][] = 'sentiment';
            $variables['classes_array'][] = $s_entity->sentiment_type;
          }
        }

      }
    }
  }
}
