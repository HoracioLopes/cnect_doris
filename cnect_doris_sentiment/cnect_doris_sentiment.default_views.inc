<?php
/**
 * @file
 * Default views definitions for 'cnect_doris_sentiment'.
 */

/**
 * Implements hook_views_default_views().
 */
function cnect_doris_sentiment_views_default_views() {

  $view = new view();
  $view->name = 'doris_node_comment_sentiment_views';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'doris_sentiment';
  $view->human_name = 'Doris Node Comment Sentiment Views';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Comment Sentiment';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
  $handler->display->display_options['exposed_form']['options']['bef'] = array(
    'general' => array(
      'allow_secondary' => 0,
      'secondary_label' => 'Advanced options',
      'collapsible_label' => NULL,
      'combine_rewrite' => NULL,
      'reset_label' => NULL,
      'bef_filter_description' => NULL,
      'any_label' => NULL,
      'filter_rewrite_values' => NULL,
    ),
    'sort' => array(
      'bef_format' => 'default',
      'advanced' => array(
        'collapsible' => 0,
        'collapsible_label' => 'Sort options',
        'combine' => 1,
        'combine_param' => 'sort',
        'combine_rewrite' => 'Sentiment Score Asc|Score (▲)
Sentiment Score Desc|Score (▼)',
        'reset' => 0,
        'reset_label' => '',
        'is_secondary' => 0,
        'secondary_label' => NULL,
        'bef_filter_description' => NULL,
        'any_label' => NULL,
        'filter_rewrite_values' => NULL,
      ),
      'secondary_label' => NULL,
      'collapsible_label' => NULL,
      'combine_rewrite' => NULL,
      'reset_label' => NULL,
      'bef_filter_description' => NULL,
      'any_label' => NULL,
      'filter_rewrite_values' => NULL,
    ),
    'title' => array(
      'bef_format' => 'default',
      'more_options' => array(
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
            'secondary_label' => NULL,
            'collapsible_label' => NULL,
            'combine_rewrite' => NULL,
            'reset_label' => NULL,
            'bef_filter_description' => NULL,
            'any_label' => NULL,
            'filter_rewrite_values' => NULL,
          ),
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
          'filter_rewrite_values' => NULL,
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
        ),
        'secondary_label' => NULL,
        'collapsible_label' => NULL,
        'combine_rewrite' => NULL,
        'reset_label' => NULL,
        'filter_rewrite_values' => NULL,
      ),
      'secondary_label' => NULL,
      'collapsible_label' => NULL,
      'combine_rewrite' => NULL,
      'reset_label' => NULL,
      'bef_filter_description' => NULL,
      'any_label' => NULL,
      'filter_rewrite_values' => NULL,
    ),
    'combine' => array(
      'bef_format' => 'default',
      'more_options' => array(
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
            'secondary_label' => NULL,
            'collapsible_label' => NULL,
            'combine_rewrite' => NULL,
            'reset_label' => NULL,
            'bef_filter_description' => NULL,
            'any_label' => NULL,
            'filter_rewrite_values' => NULL,
          ),
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
          'filter_rewrite_values' => NULL,
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
        ),
        'secondary_label' => NULL,
        'collapsible_label' => NULL,
        'combine_rewrite' => NULL,
        'reset_label' => NULL,
        'filter_rewrite_values' => NULL,
      ),
      'secondary_label' => NULL,
      'collapsible_label' => NULL,
      'combine_rewrite' => NULL,
      'reset_label' => NULL,
      'bef_filter_description' => NULL,
      'any_label' => NULL,
      'filter_rewrite_values' => NULL,
    ),
    'sentiment_type' => array(
      'bef_format' => 'default',
      'more_options' => array(
        'bef_select_all_none' => FALSE,
        'bef_collapsible' => 0,
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
            'secondary_label' => NULL,
            'collapsible_label' => NULL,
            'combine_rewrite' => NULL,
            'reset_label' => NULL,
            'bef_filter_description' => NULL,
            'any_label' => NULL,
            'filter_rewrite_values' => NULL,
          ),
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
          'filter_rewrite_values' => NULL,
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
        ),
        'secondary_label' => NULL,
        'collapsible_label' => NULL,
        'combine_rewrite' => NULL,
        'reset_label' => NULL,
        'filter_rewrite_values' => NULL,
      ),
      'secondary_label' => NULL,
      'collapsible_label' => NULL,
      'combine_rewrite' => NULL,
      'reset_label' => NULL,
      'bef_filter_description' => NULL,
      'any_label' => NULL,
      'filter_rewrite_values' => NULL,
    ),
    'secondary_label' => NULL,
    'collapsible_label' => NULL,
    'combine_rewrite' => NULL,
    'reset_label' => NULL,
    'bef_filter_description' => NULL,
    'any_label' => NULL,
    'filter_rewrite_values' => NULL,
  );
  $handler->display->display_options['exposed_form']['options']['input_required'] = 0;
  $handler->display->display_options['exposed_form']['options']['text_input_required_format'] = 'filtered_html';
  $handler->display->display_options['pager']['type'] = 'mini';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['tags']['previous'] = '‹ ';
  $handler->display->display_options['pager']['options']['tags']['next'] = ' ›';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Doris Sentiment: Comment */
  $handler->display->display_options['relationships']['comment_cid']['id'] = 'comment_cid';
  $handler->display->display_options['relationships']['comment_cid']['table'] = 'doris_sentiment';
  $handler->display->display_options['relationships']['comment_cid']['field'] = 'comment_cid';
  $handler->display->display_options['relationships']['comment_cid']['required'] = TRUE;
  /* Relationship: Comment: Content */
  $handler->display->display_options['relationships']['nid']['id'] = 'nid';
  $handler->display->display_options['relationships']['nid']['table'] = 'comment';
  $handler->display->display_options['relationships']['nid']['field'] = 'nid';
  $handler->display->display_options['relationships']['nid']['relationship'] = 'comment_cid';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['relationship'] = 'nid';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Doris Sentiment: Sentiment Type */
  $handler->display->display_options['fields']['sentiment_type']['id'] = 'sentiment_type';
  $handler->display->display_options['fields']['sentiment_type']['table'] = 'doris_sentiment';
  $handler->display->display_options['fields']['sentiment_type']['field'] = 'sentiment_type';
  $handler->display->display_options['fields']['sentiment_type']['label'] = 'Sentiment';
  $handler->display->display_options['fields']['sentiment_type']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['sentiment_type']['alter']['path_case'] = 'upper';
  /* Field: Doris Sentiment: Sentiment Score */
  $handler->display->display_options['fields']['sentiment_score']['id'] = 'sentiment_score';
  $handler->display->display_options['fields']['sentiment_score']['table'] = 'doris_sentiment';
  $handler->display->display_options['fields']['sentiment_score']['field'] = 'sentiment_score';
  $handler->display->display_options['fields']['sentiment_score']['label'] = 'Score';
  $handler->display->display_options['fields']['sentiment_score']['precision'] = '0';
  $handler->display->display_options['fields']['sentiment_score']['separator'] = '';
  /* Field: User: Username */
  $handler->display->display_options['fields']['username']['id'] = 'username';
  $handler->display->display_options['fields']['username']['table'] = 'users';
  $handler->display->display_options['fields']['username']['field'] = 'username';
  $handler->display->display_options['fields']['username']['relationship'] = 'uid';
  $handler->display->display_options['fields']['username']['label'] = '';
  $handler->display->display_options['fields']['username']['exclude'] = TRUE;
  $handler->display->display_options['fields']['username']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['username']['link_to_user'] = FALSE;
  /* Field: Realname: Real name */
  $handler->display->display_options['fields']['realname']['id'] = 'realname';
  $handler->display->display_options['fields']['realname']['table'] = 'realname';
  $handler->display->display_options['fields']['realname']['field'] = 'realname';
  $handler->display->display_options['fields']['realname']['relationship'] = 'uid';
  $handler->display->display_options['fields']['realname']['label'] = '';
  $handler->display->display_options['fields']['realname']['exclude'] = TRUE;
  $handler->display->display_options['fields']['realname']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['realname']['link_to_user'] = FALSE;
  /* Sort criterion: Doris Sentiment: Sentiment Score */
  $handler->display->display_options['sorts']['sentiment_score']['id'] = 'sentiment_score';
  $handler->display->display_options['sorts']['sentiment_score']['table'] = 'doris_sentiment';
  $handler->display->display_options['sorts']['sentiment_score']['field'] = 'sentiment_score';
  $handler->display->display_options['sorts']['sentiment_score']['order'] = 'DESC';
  $handler->display->display_options['sorts']['sentiment_score']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['sentiment_score']['expose']['label'] = 'Sentiment Score';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['relationship'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Global: Combine fields filter */
  $handler->display->display_options['filters']['combine']['id'] = 'combine';
  $handler->display->display_options['filters']['combine']['table'] = 'views';
  $handler->display->display_options['filters']['combine']['field'] = 'combine';
  $handler->display->display_options['filters']['combine']['operator'] = 'contains';
  $handler->display->display_options['filters']['combine']['group'] = 1;
  $handler->display->display_options['filters']['combine']['exposed'] = TRUE;
  $handler->display->display_options['filters']['combine']['expose']['operator_id'] = 'combine_op';
  $handler->display->display_options['filters']['combine']['expose']['label'] = 'Author';
  $handler->display->display_options['filters']['combine']['expose']['operator'] = 'combine_op';
  $handler->display->display_options['filters']['combine']['expose']['identifier'] = 'combine';
  $handler->display->display_options['filters']['combine']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    10 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
  );
  $handler->display->display_options['filters']['combine']['fields'] = array(
    'username' => 'username',
    'realname' => 'realname',
  );
  /* Filter criterion: Comment: Approved */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'comment';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['relationship'] = 'entity_id';
  $handler->display->display_options['filters']['status']['value'] = '1';

  /* Display: Positive */
  $handler = $view->new_display('block', 'Positive', 'doris_sentiment_comment_positive');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Positive';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'mini';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '1';
  $handler->display->display_options['pager']['options']['tags']['previous'] = '‹ ';
  $handler->display->display_options['pager']['options']['tags']['next'] = ' ›';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Doris Sentiment: Sentiment Type */
  $handler->display->display_options['fields']['sentiment_type']['id'] = 'sentiment_type';
  $handler->display->display_options['fields']['sentiment_type']['table'] = 'doris_sentiment';
  $handler->display->display_options['fields']['sentiment_type']['field'] = 'sentiment_type';
  $handler->display->display_options['fields']['sentiment_type']['label'] = 'Type';
  $handler->display->display_options['fields']['sentiment_type']['exclude'] = TRUE;
  $handler->display->display_options['fields']['sentiment_type']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['sentiment_type']['alter']['path_case'] = 'upper';
  /* Field: Doris Sentiment: Sentiment Score */
  $handler->display->display_options['fields']['sentiment_score']['id'] = 'sentiment_score';
  $handler->display->display_options['fields']['sentiment_score']['table'] = 'doris_sentiment';
  $handler->display->display_options['fields']['sentiment_score']['field'] = 'sentiment_score';
  $handler->display->display_options['fields']['sentiment_score']['label'] = 'Score';
  $handler->display->display_options['fields']['sentiment_score']['precision'] = '0';
  $handler->display->display_options['fields']['sentiment_score']['separator'] = '';
  /* Field: Comment: Rendered Comment */
  $handler->display->display_options['fields']['rendered_entity']['id'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['table'] = 'views_entity_comment';
  $handler->display->display_options['fields']['rendered_entity']['field'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['relationship'] = 'comment_cid';
  $handler->display->display_options['fields']['rendered_entity']['label'] = '';
  $handler->display->display_options['fields']['rendered_entity']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['rendered_entity']['link_to_entity'] = 1;
  $handler->display->display_options['fields']['rendered_entity']['display'] = 'view';
  $handler->display->display_options['fields']['rendered_entity']['view_mode'] = 'full';
  $handler->display->display_options['fields']['rendered_entity']['bypass_access'] = 0;
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Doris Sentiment: Sentiment Score */
  $handler->display->display_options['sorts']['sentiment_score']['id'] = 'sentiment_score';
  $handler->display->display_options['sorts']['sentiment_score']['table'] = 'doris_sentiment';
  $handler->display->display_options['sorts']['sentiment_score']['field'] = 'sentiment_score';
  $handler->display->display_options['sorts']['sentiment_score']['order'] = 'DESC';
  $handler->display->display_options['sorts']['sentiment_score']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['sentiment_score']['expose']['label'] = 'Sentiment Score';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Doris Sentiment: Sentiment Type */
  $handler->display->display_options['filters']['sentiment_type']['id'] = 'sentiment_type';
  $handler->display->display_options['filters']['sentiment_type']['table'] = 'doris_sentiment';
  $handler->display->display_options['filters']['sentiment_type']['field'] = 'sentiment_type';
  $handler->display->display_options['filters']['sentiment_type']['value'] = array(
    'positive' => 'positive',
  );
  $handler->display->display_options['filters']['sentiment_type']['expose']['operator_id'] = 'sentiment_type_op';
  $handler->display->display_options['filters']['sentiment_type']['expose']['label'] = 'Sentiment type';
  $handler->display->display_options['filters']['sentiment_type']['expose']['operator'] = 'sentiment_type_op';
  $handler->display->display_options['filters']['sentiment_type']['expose']['identifier'] = 'sentiment_type';
  $handler->display->display_options['filters']['sentiment_type']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    10 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
  );

  /* Display: Neutral */
  $handler = $view->new_display('block', 'Neutral', 'doris_sentiment_comment_neutral');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Neutral';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'mini';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '2';
  $handler->display->display_options['pager']['options']['tags']['previous'] = '‹ ';
  $handler->display->display_options['pager']['options']['tags']['next'] = ' ›';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Doris Sentiment: Sentiment Type */
  $handler->display->display_options['fields']['sentiment_type']['id'] = 'sentiment_type';
  $handler->display->display_options['fields']['sentiment_type']['table'] = 'doris_sentiment';
  $handler->display->display_options['fields']['sentiment_type']['field'] = 'sentiment_type';
  $handler->display->display_options['fields']['sentiment_type']['label'] = 'Type';
  $handler->display->display_options['fields']['sentiment_type']['exclude'] = TRUE;
  $handler->display->display_options['fields']['sentiment_type']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['sentiment_type']['alter']['path_case'] = 'upper';
  /* Field: Doris Sentiment: Sentiment Score */
  $handler->display->display_options['fields']['sentiment_score']['id'] = 'sentiment_score';
  $handler->display->display_options['fields']['sentiment_score']['table'] = 'doris_sentiment';
  $handler->display->display_options['fields']['sentiment_score']['field'] = 'sentiment_score';
  $handler->display->display_options['fields']['sentiment_score']['label'] = 'Score';
  $handler->display->display_options['fields']['sentiment_score']['precision'] = '0';
  $handler->display->display_options['fields']['sentiment_score']['separator'] = '';
  /* Field: Comment: Rendered Comment */
  $handler->display->display_options['fields']['rendered_entity']['id'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['table'] = 'views_entity_comment';
  $handler->display->display_options['fields']['rendered_entity']['field'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['relationship'] = 'comment_cid';
  $handler->display->display_options['fields']['rendered_entity']['label'] = '';
  $handler->display->display_options['fields']['rendered_entity']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['rendered_entity']['link_to_entity'] = 1;
  $handler->display->display_options['fields']['rendered_entity']['display'] = 'view';
  $handler->display->display_options['fields']['rendered_entity']['view_mode'] = 'full';
  $handler->display->display_options['fields']['rendered_entity']['bypass_access'] = 0;
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Doris Sentiment: Sentiment Score */
  $handler->display->display_options['sorts']['sentiment_score']['id'] = 'sentiment_score';
  $handler->display->display_options['sorts']['sentiment_score']['table'] = 'doris_sentiment';
  $handler->display->display_options['sorts']['sentiment_score']['field'] = 'sentiment_score';
  $handler->display->display_options['sorts']['sentiment_score']['order'] = 'DESC';
  $handler->display->display_options['sorts']['sentiment_score']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['sentiment_score']['expose']['label'] = 'Sentiment Score';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Doris Sentiment: Sentiment Type */
  $handler->display->display_options['filters']['sentiment_type']['id'] = 'sentiment_type';
  $handler->display->display_options['filters']['sentiment_type']['table'] = 'doris_sentiment';
  $handler->display->display_options['filters']['sentiment_type']['field'] = 'sentiment_type';
  $handler->display->display_options['filters']['sentiment_type']['value'] = array(
    'neutral' => 'neutral',
  );
  $handler->display->display_options['filters']['sentiment_type']['expose']['operator_id'] = 'sentiment_type_op';
  $handler->display->display_options['filters']['sentiment_type']['expose']['label'] = 'Sentiment type';
  $handler->display->display_options['filters']['sentiment_type']['expose']['operator'] = 'sentiment_type_op';
  $handler->display->display_options['filters']['sentiment_type']['expose']['identifier'] = 'sentiment_type';
  $handler->display->display_options['filters']['sentiment_type']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    10 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
  );

  /* Display: Negative */
  $handler = $view->new_display('block', 'Negative', 'doris_sentiment_comment_negative');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Negative';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'mini';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '3';
  $handler->display->display_options['pager']['options']['tags']['previous'] = '‹ ';
  $handler->display->display_options['pager']['options']['tags']['next'] = ' ›';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Doris Sentiment: Sentiment Type */
  $handler->display->display_options['fields']['sentiment_type']['id'] = 'sentiment_type';
  $handler->display->display_options['fields']['sentiment_type']['table'] = 'doris_sentiment';
  $handler->display->display_options['fields']['sentiment_type']['field'] = 'sentiment_type';
  $handler->display->display_options['fields']['sentiment_type']['label'] = 'Type';
  $handler->display->display_options['fields']['sentiment_type']['exclude'] = TRUE;
  $handler->display->display_options['fields']['sentiment_type']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['sentiment_type']['alter']['path_case'] = 'upper';
  /* Field: Doris Sentiment: Sentiment Score */
  $handler->display->display_options['fields']['sentiment_score']['id'] = 'sentiment_score';
  $handler->display->display_options['fields']['sentiment_score']['table'] = 'doris_sentiment';
  $handler->display->display_options['fields']['sentiment_score']['field'] = 'sentiment_score';
  $handler->display->display_options['fields']['sentiment_score']['label'] = 'Score';
  $handler->display->display_options['fields']['sentiment_score']['precision'] = '0';
  $handler->display->display_options['fields']['sentiment_score']['separator'] = '';
  /* Field: Comment: Rendered Comment */
  $handler->display->display_options['fields']['rendered_entity']['id'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['table'] = 'views_entity_comment';
  $handler->display->display_options['fields']['rendered_entity']['field'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['relationship'] = 'comment_cid';
  $handler->display->display_options['fields']['rendered_entity']['label'] = '';
  $handler->display->display_options['fields']['rendered_entity']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['rendered_entity']['link_to_entity'] = 1;
  $handler->display->display_options['fields']['rendered_entity']['display'] = 'view';
  $handler->display->display_options['fields']['rendered_entity']['view_mode'] = 'full';
  $handler->display->display_options['fields']['rendered_entity']['bypass_access'] = 0;
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Doris Sentiment: Sentiment Score */
  $handler->display->display_options['sorts']['sentiment_score']['id'] = 'sentiment_score';
  $handler->display->display_options['sorts']['sentiment_score']['table'] = 'doris_sentiment';
  $handler->display->display_options['sorts']['sentiment_score']['field'] = 'sentiment_score';
  $handler->display->display_options['sorts']['sentiment_score']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['sentiment_score']['expose']['label'] = 'Sentiment Score';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Doris Sentiment: Sentiment Type */
  $handler->display->display_options['filters']['sentiment_type']['id'] = 'sentiment_type';
  $handler->display->display_options['filters']['sentiment_type']['table'] = 'doris_sentiment';
  $handler->display->display_options['filters']['sentiment_type']['field'] = 'sentiment_type';
  $handler->display->display_options['filters']['sentiment_type']['value'] = array(
    'negative' => 'negative',
  );
  $handler->display->display_options['filters']['sentiment_type']['expose']['operator_id'] = 'sentiment_type_op';
  $handler->display->display_options['filters']['sentiment_type']['expose']['label'] = 'Sentiment type';
  $handler->display->display_options['filters']['sentiment_type']['expose']['operator'] = 'sentiment_type_op';
  $handler->display->display_options['filters']['sentiment_type']['expose']['identifier'] = 'sentiment_type';
  $handler->display->display_options['filters']['sentiment_type']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    10 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
  );
  $views[$view->name] = $view;

  return $views;
}
