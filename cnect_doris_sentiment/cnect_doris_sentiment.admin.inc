<?php
/**
 * @file
 * Administration pages for cnect_doris_sentiment module.
 */

/**
 * Implements hook_menu().
 */
function cnect_doris_sentiment_menu() {

  $items = array();

  $items['admin/config/doris/sentiment_analysis'] = array(
    'title' => 'Sentiment Analysis',
    'description' => 'Configuration page for the DORIS Sentiment Analysis.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('cnect_doris_sentiment_admin'),
    'access arguments' => array('administer doris'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
  );

  $items['node/%node/doris_comment_sentiment'] = array(
    'title' => t('DORIS'),
    'page callback' => 'cnect_doris_sentiment_report',
    'page arguments' => array('node', 1),
    'access callback' => array('cnect_doris_sentiment_report_access'),
    'access arguments' => array('node', 1),
    'type' => MENU_LOCAL_TASK,
    'weight' => 100,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function cnect_doris_sentiment_permission() {
  return array(
    'access main doris sentiment report' => array(
      'title' => t("Access main Doris sentiment report page"),
      'description' => t('Access the main Doris sentiment report page.'),
    ),
    'access doris sentiment report' => array(
      'title' => t('Access Doris node report'),
      'description' => t('Access Doris node comments sentiment report on node pages.'),
    ),
  );
}

/**
 * Access callback to the functions.
 */
function cnect_doris_sentiment_report_access($entity_type, $entity) {
  if ($entity_type == 'node' && $entity->comment_count) {
    // Main permission check.
    $access = user_access('access doris sentiment report');

    // Create a hook to alter access to the reports.
    drupal_alter('cnect_doris_sentiment_report_access', $entity_type, $entity, $access);
  }
  else {
    $access = FALSE;
  }
  return $access;
}

/**
 * Page callback for entity report on sentiment.
 */
function cnect_doris_sentiment_report($entity_type, $entity) {
  // @TODO: reports for all kinds of entity types.
  switch ($entity_type) {
    case 'node':
      $variables['entity_info']['entity'] = $entity;
      $variables['entity_info']['entity_type'] = $entity_type;
      list($variables['entity_info']['id'],,) = entity_extract_ids($entity_type, $entity);

      $report['average_comment_sentiment'] = array(
        '#markup' => theme('doris_comment_sentiment', $variables),
      );

      return drupal_render($report);
  }
}

/**
 * Callback for DORIS Sentiment settings page.
 */
function cnect_doris_sentiment_admin() {
  $form = array();

  // @TODO: handle all kinds of entity types.
  $fields = array();
  $enabled_entity_types = variable_get('cnect_doris_sentiment_enabled_entity_types', array('comment'));
  foreach ($enabled_entity_types as $entity_type) {
    $fields += _cnect_doris_get_text_fields($entity_type);
  }

  $ts_header = array(
    'field_name' => 'Bundle & Field Name',
  );

  foreach ($fields as $name => $definition) {
    foreach ($definition['bundles'] as $key => $bundles) {
      foreach ($bundles as $bundle) {
        $ts_values["{$key}:{$bundle}:{$name}"] = array(
          'field_name' => "{$key} : {$bundle} : {$name}",
        );
      }
    }
  }

  $form['sentiment'] = array(
    '#type' => 'fieldset',
    '#title' => t("Sentiment analysis"),
    '#tree' => TRUE,
  );

  $form['sentiment']['enable'] = array(
    '#type' => 'checkbox',
    '#title' => t("Enable Sentiment Analysis"),
    '#description' => t("Check this box to enable comment sentiment analysis."),
    '#default_value' => variable_get('cnect_doris_sentiment_enabled'),
  );

  $enabled_comment_types = variable_get('cnect_doris_sentiment_enabled_fields', array());
  $form['sentiment']['sentiment_table'] = array(
    '#type' => 'tableselect',
    '#header' => $ts_header,
    '#options' => $ts_values,
    '#default_value' => $enabled_comment_types,
    '#empty' => t('No text fields found.'),
  );

  $form['sentiment']['cnect_doris_sentiment_comments_per_cron_run'] = array(
    '#title' => t("Per cron run"),
    '#type' => 'textfield',
    '#description' => t("Number of comments to process per cron run"),
    '#default_value' => variable_get('cnect_doris_sentiment_comments_per_cron_run', 50),
  );

  $form['#submit'][] = 'cnect_doris_sentiment_admin_submit';

  return system_settings_form($form);
}

/**
 * Admin form submit handler.
 */
function cnect_doris_sentiment_admin_submit($form, $form_state) {
  $enabled = $form_state['values']['sentiment']['enable'];
  variable_set('cnect_doris_sentiment_enabled', $enabled);

  $enabled_fields = array_filter($form_state['values']['sentiment']['sentiment_table']);
  variable_set('cnect_doris_sentiment_enabled_fields', $enabled_fields);

  $per_cron_run = $form_state['values']['sentiment']['cnect_doris_sentiment_comments_per_cron_run'];
  variable_set('cnect_doris_sentiment_comments_per_cron_run', $per_cron_run);
}
