<?php
/**
 * @file
 * Views definitions for 'cnect_doris_sentiment'.
 */

/**
 * Implements hook_views_data().
 */
function cnect_doris_sentiment_views_data() {

  $data['doris_sentiment']['table']['group'] = t("Doris Sentiment");
  $data['doris_sentiment']['table']['base'] = array(
    'field' => 'id',
    'title' => t('Doris Sentiment Table'),
    'help' => t('Example table contains example content and can be related to comments.'),
    'weight' => -10,
  );

  // @TODO: process all kinds of entity types.
  $entity_types = variable_get('cnect_doris_sentiment_enabled_entity_types', array('comment'));

  foreach ($entity_types as $entity_type) {
    $entity_info = entity_get_info($entity_type);
    if ($entity_info['fieldable']) {
      if (isset($entity_info['base table'])) {
        $entity = $entity_info['label'];
        if ($entity == t('Node')) {
          $entity = t('Content');
        }
        $field_name = $entity_info['base table'] . '_' . $entity_info['entity keys']['id'];

        $parameters = array('@entity' => $entity);

        $data['doris_sentiment'][$field_name]['relationship'] = array(
          'handler' => 'views_handler_relationship',
          'join_handler' => 'CnectDorisSentiment_views_handler_doris_sentiment_join_handler',
          'real field' => 'entity_id',
          'base' => $entity_info['base table'],
          'base field' => $entity_info['entity keys']['id'],
          'label' => t('Sentiment related @entity', $parameters),
          'title' => $entity,
          'help' => t('A bridge to the @entity entity related to the sentiment', $parameters),
        );
      }
    }
  }

  $data['doris_sentiment']['sentiment_score'] = array(
    'title' => t("Sentiment Score"),
    'help' => t('Sentiment score value.'),

    'field' => array(
      'handler' => 'CnectDorisSentiment_views_handler_field_numeric_float_negative',
      'click sortable' => TRUE,
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
      'help' => 'Sort on related comment id field.',
    ),
  );

  $data['doris_sentiment']['sentiment_type'] = array(
    'title' => t("Sentiment Type"),
    'help' => t('Sentiment type value.'),

    'field' => array(
      'handler' => 'CnectDorisSentiment_views_handler_field_sentiment_type',
      'click sortable' => FALSE,
    ),
    'filter' => array(
      'handler' => 'CnectDorisSentiment_views_handler_filter_sentiment_type',
      'help' => 'Filter by sentiment type.',
    ),
  );

  return $data;
}
