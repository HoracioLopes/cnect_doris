-- SUMMARY --

This module provides a bridge between Drupal and DORIS webservice.
The main module (cnect_doris) doesn't do anything on it's own.
To enable further functionality just enable one of the submodules.


-- INSTALLATION --

* Install as usual, see http://drupal.org/documentation/install/modules-themes/modules-7
  for further information.


-- CONFIGURATION --

* Config the webservice path at admin/config/doris


-- TO DO --

* Handle all kinds of entities.
* Prepare a text representation of the entity before sending to process.
