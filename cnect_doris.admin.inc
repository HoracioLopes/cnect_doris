<?php
/**
 * @file
 * Administration screens for DORIS.
 */

/**
 * Implements hook_menu().
 */
function cnect_doris_menu() {

  $items = array();

  $items['admin/config/doris'] = array(
    'title' => 'DORIS Services',
    'description' => 'Configuration page for the DORIS Services.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('cnect_doris_admin'),
    'access arguments' => array('administer doris'),
  );

  $items['admin/config/doris/service'] = array(
    'title' => 'DORIS Services',
    'description' => 'Configuration page for the DORIS Services.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('cnect_doris_admin'),
    'access arguments' => array('administer doris'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 1,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function cnect_doris_permission() {
  return array(
    'administer doris' => array(
      'title' => t('Administer Doris'),
      'description' => t('Perform administration tasks for Doris module.'),
    ),
  );
}

/**
 * Callback for DORIS settings page.
 */
function cnect_doris_admin() {
  $form = array();

  $form['cnect_doris_server_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Doris Server Hostname'),
    '#default_value' => variable_get('cnect_doris_server_host', ''),
    '#description' => t("The hostname or IP address for the DORIS webservice."),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
